let app = new Vue({
    el:'#app',
    data: {
        message: 'Hello Vue!!'
    }

});

let app2 = new Vue({
    el: '#app-2',
    data: {
        message:'aho da nya' + new Date().toLocaleString()
    }
});

let app3 = new Vue({
    el: '#app-3',
    data: {
        seen: true
    }
});

let app4 = new Vue({
    el: '#app-4',
    data: {
        todos: [
            { text: 'Learn JavaScript' },
            { text: 'Learn Vue' },
            { text: 'Build something awesome' }
        ]
    }
});

const app5 = new Vue({
    el: '#app-5',
    data: {
        message: 'はろ－びゅーじぇいえす!'
    },
    methods: {
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('')
        }
    }
});

const app6 = new Vue({
    el: '#app-6',
    data: {
        message: 'すきにかえてね！'
    }
});

Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
})

const app7 = new Vue({
    el: '#app-7',
    data: {
        groceryList: [
            { id: 0, text: 'Vegetables' },
            { id: 1, text: 'Cheese' },
            { id: 2, text: 'Whatever else humans are supposed to eat' }
        ]
    }
});




